# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json
import azure.functions as func
import azure.durable_functions as df
from collections import Counter


def orchestrator_function(context: df.DurableOrchestrationContext):
    app =[]
    input = yield context.call_activity('GetInputDataFn', None)
    mappers = []
    for ret in input:
        mappers.append(context.call_activity('Mapper', ret))
    result = yield context.task_all(mappers)
    result1 = yield context.call_activity('Shuffler', result)
    print(result1)
    reducers = []
    for rets in result1:
        reducers.append(context.call_activity('Reducer', rets))
    result2 = yield context.task_all(reducers)
    counter = Counter(result2)
    print(counter)
    return counter
main = df.Orchestrator.create(orchestrator_function)