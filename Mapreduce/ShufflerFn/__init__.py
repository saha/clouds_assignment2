# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
from collections import defaultdict

def main(pairs: list[tuple[str, int]]) -> tuple[str, int]:
    output = defaultdict(list)
    items =[]
    shufflerkey =[]
    for pair in pairs:
        keys = pair.keys()
        values = pair.values()
        for k, v in zip(keys, values):
            items.append((k,v))
    return items

