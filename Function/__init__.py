import logging
import math
import azure.functions as func
def BlackBoxFunction(x):
    f = math.sin(x)
    return f
def main(req: func.HttpRequest) -> func.HttpResponse:
    a = req.params.get('LENGTH')
    b = req.params.get('WIDTH')
    var = " "
    x = " "
    LENGTH = float(b)
    WIDTH = float(a)
    N = [10, 100, 1000, 10000, 100000, 1000000, 10000000]
    l = len(N)
    for i in range(0, l):
        integral = 0.0
        dx = WIDTH-LENGTH/N[i]
        for j in range(1, N[i]):
            xp = dx*(j+0.5)
            di = BlackBoxFunction(xp)*dx
            integral += di   
        var += str(integral)+","
        x = var
    return func.HttpResponse(x)

